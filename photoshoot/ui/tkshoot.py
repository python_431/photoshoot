#!/usr/bin/env python3
# sudo apt install python3-tk

import tkinter as tk

from photoshoot.model.files import Photo

mainwin = tk.Tk()

title = tk.Label(mainwin, text="All Pictures")
info  = tk.Label(mainwin, text=" \n ")

photoids = list(Photo.getids())
allphotos = { id:Photo.fromdb(id) for id in photoids }

listbox = tk.Listbox(mainwin)
for i,(id,p) in enumerate(allphotos.items(), 1):
    listbox.insert(i, p.name) 

def photoselect():
    #python 3.8+ : if sel := listbox.curselection():
    sel = listbox.curselection()
    if sel:
        i, = sel # first item i = sel[0]
        p = allphotos[photoids[i]]
        infotext = f"{p.name}\n{p.date}" 
    else:
        infotext = "Please select a picture!\n" 
    info.config(text=infotext)

validbutton = tk.Button(mainwin,text="Details",command=photoselect)
quitbutton = tk.Button(mainwin,text="Quit",command=mainwin.quit)

title.pack() 
listbox.pack()
info.pack()
validbutton.pack()
quitbutton.pack()

mainwin.mainloop() 
