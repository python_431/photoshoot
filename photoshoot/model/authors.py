#!/usr/bin/env python3

from photoshoot.model.db.sql import DbObject

class Author(DbObject):
     table = 'authors'
     dbfields = [ 'id', 'first', 'last' ]
     def __init__(self, id=None, first=None, last=None):
         self.id = id
         self.first = first
         self.last  = last

