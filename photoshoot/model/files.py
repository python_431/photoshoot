#!/usr/bin/env python3
'''Module handling model for all kind of Files, Photos, etc.'''

import datetime

from photoshoot.model.db.sql import DbObject

class DataFile:
    '''Abstract class handling common behaviour for photos, videos, ...'''
    # TODO: use dataclass from std lib
    dbfields = [ 'name', 'date', 'author', 'filepath',
            'lat', 'long', 'city', 'shoot' ]
    def __init__(self, name=None, date=None, author=None,
            filepath=None, lat=None, long=None, city=None, shoot=None, /, id=None):
        if id is not None:
            self.id = id
        self.name = name
        self.date  = datetime.date.today() if date is None else date
        self.author = author
        self.filepath = filepath
        self.lat = lat
        self.long = long
        self.city = city
        self.shoot = shoot
    def __eq__(self, other):
        return type(self) is type(other) and \
                hasattr(self,'id') and hasattr(other,'id') and \
                self.id == other.id

# NOTE: DbObject is an auxiliary class
class Photo(DataFile, DbObject):
    table = 'photos'

class Video(DataFile, DbObject):
    table = 'videos'
