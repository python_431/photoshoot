#!/usr/bin/env python3
'''Module handling photo and video shoots...'''

# Exercise : add stuff to be able to read/insert/update
# shoots into database (just ignore fields that are not in db)

# let's have a coffee break!
# exercise at 11:30

import datetime

from photoshoot.model.db.sql import DbObject
from photoshoot.model.files import Photo, Video

class Shoot(DbObject):
    table = 'shoots'
    dbfields = [ 'name', 'date' ] 
    def __init__(self, name=None, date=None, duration=None, /,
            id=None):
        if id is not None:
            self.id = id
            self.files  = self.foreign_fromdb(Photo, 'shoot') 
            self.files += self.foreign_fromdb(Video, 'shoot') 
        self.name  = name
        self.date  = datetime.date.today()
        self.duration = duration
