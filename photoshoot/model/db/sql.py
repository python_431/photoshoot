#!/usr/bin/env python3
'''SQL Helper functions and auxiliary class for common requests.
   concrete classes must have table and fields attributes.'''

import sqlite3
from pathlib import Path
from photoshoot.model.exceptions import PhotoShootConfError
from contextlib import contextmanager

from photoshoot.model.conf import dbfile

# TODO: use connections pool
# on a real database server

@contextmanager
def dbconnect():
    # TODO: test also if we can READ the file
    # by handling exceptions
    if not Path(dbfile).is_file():
        raise PhotoShootConfError(f"DB file {dbfile} not found.")
    conn = sqlite3.connect(dbfile)
    try:
        yield conn
    finally:
        conn.close()

def get_ids_from_db(cls):
    sql = f"SELECT id FROM {cls.table}"
    with dbconnect() as conn:
        curs = conn.cursor()
        curs.execute(sql)
        # Using iterator is BAD with sqlite :
        # it's locking the database if the "connection"
        # is not closed!!!! (Ok for client/server SGBDR)
        #for (id,) in curs:
        #    yield id
        return [ id for (id,) in curs ] 

def read_entity_from_db(cls, id):
    fields = ','.join(cls.dbfields)
    sql = f"SELECT {fields} FROM {cls.table}" \
            " WHERE id = ?"
    with dbconnect() as conn:
        curs = conn.cursor()
        curs.execute(sql, (id,) )
        row = curs.fetchone()
        return row

def read_simple_join_on_column(obj, table, col, id):
     sql = f"SELECT id FROM {table}" \
           f" WHERE {col} = ?"
     with dbconnect() as conn:
        curs = conn.cursor()
        curs.execute(sql, (id,) )
        return [ id for (id,) in curs ]

def insert_entity_in_db(obj):
    # TODO: check error (no constraints so far)
    qmarks = ','.join( ['?'] * len(obj.dbfields) )
    fields = ','.join(obj.dbfields)
    row = [ getattr(obj, f) for f in obj.dbfields ]
    sql = f"INSERT INTO {obj.table}({fields})" \
            f"VALUES({qmarks})"
    with dbconnect() as conn:
        curs = conn.cursor()
        curs.execute(sql, row)
        id = curs.lastrowid
        conn.commit()
        return id

def update_entity_in_db(obj):
    # TODO: test if id is in db
    dbfields = obj.dbfields
    row = [ getattr(obj, f) for f in dbfields ]
    sql = f"UPDATE {obj.table} SET " + \
            ', '.join( ( f"{f} = ? " for f in dbfields ) ) + \
            'WHERE id = ?'
    with dbconnect() as conn:
        curs = conn.cursor()
        curs.execute(sql, [*row, obj.id])
        conn.commit()

class DbObject:
    @classmethod
    def getids(cls):
        return get_ids_from_db(cls)
    @classmethod
    def fromdb(cls, id):
        row = read_entity_from_db(cls, id)
        obj = cls(*row, id=id)
        return obj
    def insertdb(self):
        id = insert_entity_in_db(self)
        self.id = id
        return id
    def updatedb(self):
        #if self.id is None:
        if not hasattr(self, 'id'):
            raise ValueError('Object not in database')
        update_entity_in_db(self)
    def foreign_fromdb(self, cls, col):
        if not hasattr(self, 'id'):
            return [] 
        return [ cls.fromdb(id) for id in \
         read_simple_join_on_column(self, cls.table, col, self.id) ]
