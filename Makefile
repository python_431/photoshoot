.PHONY: help venv test cover clean mrproper

help:
	@echo "Targets: venv test cover clean mrproper"

venv:
	if [ ! -d "venv" ]; then             \
	    python -m venv venv; 	     \
	    . venv/bin/activate; 	     \
	    pip install --upgrade pip;       \
	    pip install -r requirements.txt; \
	fi

test:
	nosetests -v tests

cover:
	nosetests -v --cover-package photoshoot --with-coverage --cover-html tests
       #	--cover-package frenchNumerals tests

clean:
	rm -rf cover __pycache__
	find photoshoot tests -type d -name __pycache__ -exec rm -r {} \;

mrproper: clean
	rm -rf venv
