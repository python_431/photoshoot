DROP TABLE IF EXISTS photos;
CREATE TABLE photos(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    date TEXT,
    author INTEGER,
    filepath TEXT,
    lat REAL, long REAL,
    city id,
    shoot id);

DROP TABLE IF EXISTS videos;
CREATE TABLE videos(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    date TEXT,
    author INTEGER,
    filepath TEXT,
    lat REAL, long REAL,
    city id,
    shoot id);


DROP TABLE IF EXISTS authors;
CREATE TABLE authors(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    first TEXT,
    last  TEXT);

DROP TABLE IF EXISTS shoots;
CREATE TABLE shoots(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    author INTEGER,
    date TEXT);
