INSERT INTO photos (name, date, author, shoot)
  VALUES ('IMG1234','2020-12-01', 1, 1),
         ('IMG3241','2019-03-16', 2, 1),
         ('IMG6661','2019-09-01', 2, 2);

INSERT INTO videos (name, date, author)
  VALUES ('VID6751', '2019-05-12', 2),
         ('VID7866', '2018-10-11', 3);

INSERT INTO authors (first, last)
  VALUES ('John', 'Cleese'),
         ('Terry', 'Gillian'),
         ('Connie', 'Booth');

INSERT INTO shoots (name, author,date)
  VALUES ('Walk in the park', 3, '2010-03-01'),
         ('Last stay in Edinburgh', 2, '2020-04-11');
