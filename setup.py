#!/usr/bin/env python3

from distutils.core import setup

setup(name='Photoshoot',
      version='0.1',
      description='Photo Studio Management Application',
      author='Jean-Pierre Messager',
      author_email='jp@xiasma.fr',
      url='https://gitlab.com/python_431/photoshoot',
      packages=['photoshoot', ],
     )
