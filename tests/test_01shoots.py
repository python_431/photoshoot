#!/usr/bin/env python3

from nose.tools import raises

import datetime

from photoshoot.model.shoots import Shoot, Photo

def test_Shoot_db_insert_and_read():
    p = Shoot('spam', '2021-03-03')
    id = p.insertdb()
    p = Shoot.fromdb(id)
    assert p.id == id
    assert p.name == 'spam'

def test_Shoot_db_import_Photos():
    s = Shoot.fromdb(1)
    p = Photo.fromdb(1)
    assert p in s.files
    assert 1 in [ pp.id for pp in s.files ] 

def test_Shoot_db_update():
    p = Shoot('spam', '2021-03-03')
    id = p.insertdb()
    p.name = 'ham'
    p.updatedb()
    p = Shoot.fromdb(id)
    assert p.id == id
    assert p.name == 'ham'

@raises(ValueError)
def test_Shoot_db_update_fail_if_not_exists():
    p = Shoot('spam', '2021-03-03')
    p.name = 'ham'
    p.updatedb()
