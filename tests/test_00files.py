#!/usr/bin/env python3

from nose.tools import raises

import datetime

#import photoshoot

from photoshoot.model.files import Photo, DataFile

def test_DataFile_attr():
    df = DataFile('spam', None, None)
    assert df.name == 'spam'
    assert df.date == datetime.date.today()
    assert df.author is None 

def test_Photo_db_insert_and_read():
    p = Photo('spam', '2021-03-03', 2, '/tmp/test.jpg') 
    id = p.insertdb()
    p = Photo.fromdb(id)
    assert p.id == id
    assert p.name == 'spam'

def test_Photo_db_update():
    p = Photo('spam', '2021-03-03', 2, '/tmp/test.jpg') 
    id = p.insertdb()
    p.name = 'ham'
    p.filepath = '/tmp/other.jpg'
    p.updatedb()
    p = Photo.fromdb(id)
    assert p.id == id
    assert p.name == 'ham'
    assert p.filepath == '/tmp/other.jpg'

@raises(ValueError)
def test_Photo_db_update_fail_if_not_exists():
    p = Photo('spam', '2021-03-03', 2, '/tmp/test.jpg') 
    p.name = 'ham'
    p.filepath = '/tmp/other.jpg'
    p.updatedb()
 
